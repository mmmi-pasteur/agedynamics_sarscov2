require(odin)

################################################################################
# Age-structured model
################################################################################
seeir_age_saveAge <- odin({
  # 'interpolate' is used to have R0 and contact matrix that vary through time
  # The option 'constant' makes it equivalent to having a switch point
  R0 <- interpolate(interp_ts_R0, interp_R0s, "constant")
  c_mat[, ] <- interpolate(interp_ts_c_mat, interp_c_mats, "constant")
  
  D <- 1. / g2 + 1. / g3 # Infectious period
  beta <- R0 / D # Transmission rate
  prop_I[] <- (E2[i] + IMild[i] + IHosp[i]) / pop[i] # Proportion infectious
  c_prop_I[, ] <- c_mat[i, j] * prop_I[j] # Contact matrix * prop infectious
  
  ##############################################################################
  # Transitions
  ##############################################################################
  # Infections
  S_to_E1[] <- beta * S[i] * sum(c_prop_I[i, ])
  E1_to_E2[] <- g1 * E1[i]
  E2_to_I[] <- g2 * E2[i]
  # Mild cases
  E2_to_IMild[] <- (1. - pHosp[i]) * E2_to_I[i]
  IMild_to_R[] <- g3 * IMild[i]
  # Hospitalized cases
  E2_to_IHosp[] <- pHosp[i] * E2_to_I[i]
  IHosp_to_IBarHosp[] <- g3 * IHosp[i]
  IBarHosp_to_H[] <- gToHosp * IBarHosp[i]
  
  
  # Infectors
  InfPairs[,] <- beta * S[i] * c_prop_I[i, j]
  Infectors[] <- sum(InfPairs[, i])
  
  ##############################################################################
  # Derivatives
  ##############################################################################
  deriv(S[]) <- -S_to_E1[i]
  deriv(E1[]) <- S_to_E1[i] - E1_to_E2[i]
  deriv(E2[]) <- E1_to_E2[i] - E2_to_I[i]
  deriv(IMild[]) <- E2_to_IMild[i] - IMild_to_R[i]
  deriv(IHosp[]) <- E2_to_IHosp[i] - IHosp_to_IBarHosp[i]
  deriv(IBarHosp[]) <- IHosp_to_IBarHosp[i] - IBarHosp_to_H[i]
  deriv(H[]) <- IBarHosp_to_H[i]
  
  ##############################################################################
  # Initial conditions
  ##############################################################################
  initial(S[]) <- S_ini[i]
  initial(E1[]) <- E1_ini[i]
  initial(E2[]) <- E2_ini[i]
  initial(IMild[]) <- IMild_ini[i]
  initial(IHosp[]) <- IHosp_ini[i]
  initial(IBarHosp[]) <- IBarHosp_ini[i]
  initial(H[]) <- H_ini[i]
  
  ##############################################################################
  # Variables to add to output
  ##############################################################################
  output(iInf) <- sum(S_to_E1) # Incident n. of infections
  output(iHosp) <- sum(IBarHosp_to_H) # Incident n. of hospitalizations
  
  
  output(iInf_Age[]) <- S_to_E1[i] # New infections by age
  output(iInfector_Age[]) <- Infectors[i] # Infections attributed to by age
  output(iHosp_Age[]) <- IBarHosp_to_H[i] # Incidence n. of hospitalizations by age
  output(iI_Age[]) <- E2_to_I[i] # Incident n. of entry in I (IMild or IHosp) by age
  
  ##############################################################################
  # User defined parameters (default in parentheses)
  ##############################################################################
  g1 <- user(1. / 4.)
  g2 <- user(1. / 1.)
  g3 <- user(1. / 3.)
  gToHosp <- user(1. / 4.)
  # gToICU <- user(1. / 3.) # NOTE: not used
  pop[] <- user()
  pHosp[] <- user()
  S_ini[] <- user()
  E1_ini[] <- user()
  E2_ini[] <- user()
  IMild_ini[] <- user()
  IHosp_ini[] <- user()
  IBarHosp_ini[] <- user()
  H_ini[] <- user()
  interp_ts_R0[] <- user()
  interp_R0s[] <- user()
  interp_ts_c_mat[] <- user()
  interp_c_mats[, , ] <- user()
  
  ##############################################################################
  # Dimensions
  ##############################################################################
  dim(pop) <- user()
  n_age <- length(pop)
  
  dim(interp_ts_R0) <- user()
  dim(interp_R0s) <- length(interp_ts_R0)
  dim(interp_ts_c_mat) <- user()
  dim(interp_c_mats) <- c(length(interp_ts_c_mat), n_age, n_age)
  
  dim(c_mat) <- c(n_age, n_age)
  dim(pHosp) <- n_age
  dim(prop_I) <- n_age
  dim(c_prop_I) <- c(n_age, n_age)
  dim(S_to_E1) <- n_age
  dim(E1_to_E2) <- n_age
  dim(E2_to_I) <- n_age
  dim(E2_to_IMild) <- n_age
  dim(IMild_to_R) <- n_age
  dim(E2_to_IHosp) <- n_age
  dim(IHosp_to_IBarHosp) <- n_age
  dim(IBarHosp_to_H) <- n_age
  dim(InfPairs) <- c(n_age, n_age)
  dim(Infectors) <- n_age
  dim(S) <- n_age
  dim(E1) <- n_age
  dim(E2) <- n_age
  dim(IMild) <- n_age
  dim(IHosp) <- n_age
  dim(IBarHosp) <- n_age
  dim(H) <- n_age
  dim(S_ini) <- n_age
  dim(E1_ini) <- n_age
  dim(E2_ini) <- n_age
  dim(IMild_ini) <- n_age
  dim(IHosp_ini) <- n_age
  dim(IBarHosp_ini) <- n_age
  dim(H_ini) <- n_age
  
  dim(iInf_Age) <- c(n_age)
  dim(iInfector_Age) <- c(n_age)
  dim(iHosp_Age) <- c(n_age)
  dim(iI_Age) <- c(n_age)
})

################################################################################
# Age-structured model with ICU admissions
################################################################################
seeir_age_ICU_saveAge <- odin({
  # 'interpolate' is used to have R0 and contact matrix that vary through time
  # The option 'constant' makes it equivalent to having a switch point
  R0 <- interpolate(interp_ts_R0, interp_R0s, "constant")
  c_mat[, ] <- interpolate(interp_ts_c_mat, interp_c_mats, "constant")
  
  D <- 1. / g2 + 1. / g3 # Infectious period
  beta <- R0 / D # Transmission rate
  prop_I[] <- (E2[i] + IMild[i] + IHosp[i]) / pop[i] # Proportion infectious
  c_prop_I[, ] <- c_mat[i, j] * prop_I[j] # Contact matrix * prop infectious
  
  ##############################################################################
  # Transitions
  ##############################################################################
  # Infections
  S_to_E1[] <- beta * S[i] * sum(c_prop_I[i, ])
  E1_to_E2[] <- g1 * E1[i]
  E2_to_I[] <- g2 * E2[i]
  # Mild cases
  E2_to_IMild[] <- (1. - pHosp[i]) * E2_to_I[i]
  IMild_to_R[] <- g3 * IMild[i]
  # Hospitalized cases
  E2_to_IHosp[] <- pHosp[i] * E2_to_I[i]
  IHosp_to_IBarHosp[] <- g3 * IHosp[i]
  IBarHosp_to_H[] <- gToHosp * IBarHosp[i]
  IBarHosp_to_HICU[] <- gToHosp * IBarHosp[i] * pICU[i]
  HICU_to_ICU[] <- gToICU * HICU[i]
  
  ##############################################################################
  # Derivatives
  ##############################################################################
  deriv(S[]) <- -S_to_E1[i]
  deriv(E1[]) <- S_to_E1[i] - E1_to_E2[i]
  deriv(E2[]) <- E1_to_E2[i] - E2_to_I[i]
  deriv(IMild[]) <- E2_to_IMild[i] - IMild_to_R[i]
  deriv(IHosp[]) <- E2_to_IHosp[i] - IHosp_to_IBarHosp[i]
  deriv(IBarHosp[]) <- IHosp_to_IBarHosp[i] - IBarHosp_to_H[i]
  deriv(H[]) <- IBarHosp_to_H[i]
  deriv(HICU[]) <- IBarHosp_to_HICU[i] - HICU_to_ICU[i]
  deriv(ICU[]) <- HICU_to_ICU[i]
  
  ##############################################################################
  # Initial conditions
  ##############################################################################
  initial(S[]) <- S_ini[i]
  initial(E1[]) <- E1_ini[i]
  initial(E2[]) <- E2_ini[i]
  initial(IMild[]) <- IMild_ini[i]
  initial(IHosp[]) <- IHosp_ini[i]
  initial(IBarHosp[]) <- IBarHosp_ini[i]
  initial(H[]) <- H_ini[i]
  initial(HICU[]) <- HICU_ini[i]
  initial(ICU[]) <- ICU_ini[i]
  
  ##############################################################################
  # Variables to add to output
  ##############################################################################
  output(iInf) <- sum(S_to_E1) # Incident n. of infections
  output(iHosp) <- sum(IBarHosp_to_H) # Incident n. of hospitalizations
  output(iICU) <- sum(HICU_to_ICU) # Incident n. of hospitalizations
  
  output(iInf_Age[]) <- S_to_E1[i] # Incident n. of infections by age
  output(iHosp_Age[]) <- IBarHosp_to_H[i] # Incidence n. of hospitalizations by age
  output(iI_Age[]) <- E2_to_I[i] # Incident n. of entry in I (IMild or IHosp) by age
  
  
  ##############################################################################
  # User defined parameters (default in parentheses)
  ##############################################################################
  g1 <- user(1. / 4.)
  g2 <- user(1. / 1.)
  g3 <- user(1. / 3.)
  gToHosp <- user(1. / 4.)
  gToICU <- user(1. / 1.5)
  
  pop[] <- user()
  pHosp[] <- user()
  pICU[] <- user()
  
  S_ini[] <- user()
  E1_ini[] <- user()
  E2_ini[] <- user()
  IMild_ini[] <- user()
  IHosp_ini[] <- user()
  IBarHosp_ini[] <- user()
  H_ini[] <- user()
  HICU_ini[] <- user()
  ICU_ini[] <- user()
  
  interp_ts_R0[] <- user()
  interp_R0s[] <- user()
  interp_ts_c_mat[] <- user()
  interp_c_mats[, , ] <- user()
  
  ##############################################################################
  # Dimensions
  ##############################################################################
  dim(pop) <- user()
  n_age <- length(pop)
  
  dim(interp_ts_R0) <- user()
  dim(interp_R0s) <- length(interp_ts_R0)
  dim(interp_ts_c_mat) <- user()
  dim(interp_c_mats) <- c(length(interp_ts_c_mat), n_age, n_age)
  
  dim(c_mat) <- c(n_age, n_age)
  dim(pHosp) <- n_age
  dim(pICU) <- n_age
  dim(prop_I) <- n_age
  dim(c_prop_I) <- c(n_age, n_age)
  dim(S_to_E1) <- n_age
  dim(E1_to_E2) <- n_age
  dim(E2_to_I) <- n_age
  dim(E2_to_IMild) <- n_age
  dim(IMild_to_R) <- n_age
  dim(E2_to_IHosp) <- n_age
  dim(IHosp_to_IBarHosp) <- n_age
  dim(IBarHosp_to_H) <- n_age
  dim(IBarHosp_to_HICU) <- n_age
  dim(HICU_to_ICU) <- n_age
  dim(S) <- n_age
  dim(E1) <- n_age
  dim(E2) <- n_age
  dim(IMild) <- n_age
  dim(IHosp) <- n_age
  dim(IBarHosp) <- n_age
  dim(H) <- n_age
  dim(HICU) <- n_age
  dim(ICU) <- n_age
  dim(S_ini) <- n_age
  dim(E1_ini) <- n_age
  dim(E2_ini) <- n_age
  dim(IMild_ini) <- n_age
  dim(IHosp_ini) <- n_age
  dim(IBarHosp_ini) <- n_age
  dim(H_ini) <- n_age
  dim(HICU_ini) <- n_age
  dim(ICU_ini) <- n_age
  
  dim(iInf_Age) <- c(n_age)
  dim(iHosp_Age) <- c(n_age)
  dim(iI_Age) <- c(n_age)
})
