# AgeDynamics_SARSCoV2

Data and source code used in [this preprint](https://hal-pasteur.archives-ouvertes.fr/pasteur-03170966). 

## Data

Data used in this analysis are available in the Data folder. The following file are used for the inference:

1. **Hospital admissions data until May 11th, 2020** (hospitalization_data/mat_daily_hospitalization_by_region_first_wave.rds)<br />
Daily counts (by row) by region (in columns).

2. **Hospital admissions data** (aggregated by age goup) from May 11th, 2020 until May 24th, 2020 (hospitalization_data/mat_daily_hospitalization_by_region_until24May2020.rds)<br />
Daily counts (by row) by region (in columns).

3. **Weekly hospital admissions data by age group and regions** (hospitalization_data/list_mat_weekly_hosp_per_age.rds)<br />
List of length n<sub>regions</sub> = 13. Each element of the list is a matrix with n<sub>age</sub> = 9 columns corresponding to the different age groups. The rows correspond to the weeks used for model calibration in the different regions (starting from May 25th, 2020). Each element of the matrix corresponds to the weekly number of hospital admissions by week of admission in hospital and by age group.

4. **Weekly test data among symptomatic individuals by age group and regions** (test_data/list_mat_weekly_symptomatic_tests_remove_ehpa.rds)<br />
List of length n<sub>regions</sub> = 13. Each element of the list is a matrix with n<sub>age</sub> = 9 columns corresponding to the different age groups. The rows correspond to the weeks used for model calibration in the different regions (starting from June 15th, 2020). Each element of the matrix corresponds to the weekly number of tests among symptomatic individuals by week of tests and by age group.

4. **Weekly positive test data among symptomatic individuals by age group and regions** (test_data/list_mat_weekly_symptomatic_positive_remove_ehpa.rds)
List of length n<sub>regions</sub> = 13. Each element of the list is a matrix with n<sub>age</sub> = 9 columns corresponding to the different age groups. The rows correspond to the weeks used for model calibration in the different regions (starting from June 15th, 2020). Each element of the matrix corresponds to the weekly number of positive tests among symptomatic individuals by week of tests and by age group.

## Scripts
Prior running any scripts, ensure to set the working directory to the source file location.

**0. Calibration of the model until May 11th, 2020 (ModelCalibration_FirstWave2020.R)**

This provides the scripts:
- to calibrate to epidemiological model on daily hospitalizations until May 11th, 2020.
- to run simulations from the posterior distribution.

The results of this analysis are not reported in the paper but are used to get the starting conditions following the release of the first lockdown on May 11th, 2020.

**1. Calibration of the model on age-stratified weekly hospital admissions and positive tests among symptomatic individuals data (mainCalibration_Age.R)**

This provides the scripts:
- to calibrate to model across the 13 metropolitan French regions.
- to run simulations from the posterior distribution.

**2. Assessment of the impact of various targeted strategies (mainSimulations_Strategies.R)**

This provides the scripts to evaluate the impact of interventions on the peak in daily new infections, the peak in daily hospitalization, the peak in daily ICU admissions, the number of deaths, the number of life years lost and the number of quality-adjusted life years lost. The script enables to run:
- strategies shielding the elderly population (reduction of 30%/50% in effective contacts in those aged 70 y.o. and older)
- strategies targeting age groups (reduction of 1 effective contacts ; reduction of 10% in effective contacts).

and to plot the results.

